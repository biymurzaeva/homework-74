const express = require('express');
const fileDB = require('../fileDB');
const router = express.Router();

router.get('/', (req, res) => {
	const messages = fileDB.getItems();
	res.send(messages);
});

router.post('/', (req, res) => {
	if (!req.body.message) {
		return res.status(404).send({error: 'Data not valid'});
	}

	const newMessage = fileDB.addItem({
		message: req.body.message
	});

	res.send(newMessage);
});

module.exports = router;