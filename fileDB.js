const fs = require('fs');
const path = './messages';
let data = {};
let listMessages = [];

module.exports = {
	init() {
		fs.readdir(path, (err, files) => {
			files.forEach(file => {
				listMessages.push(file);
			});
			console.log(listMessages.slice(listMessages.length - 5));
		});
	},
	getItems() {
		return listMessages.slice(listMessages.length - 5);
	},
	addItem(item) {
		const date = new Date();
		item.datetime = date.toISOString();
		data = item;
		this.save();
		return item;
	},
	save() {
		fs.writeFileSync(`./messages/${data.datetime}.json`, JSON.stringify(data));
	}
};